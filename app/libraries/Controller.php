<?php
/**
 * Base Controller
 * Handles loading of models and views
 */

class Controller{
    // Load model
    public function model($model){
        // Require model file
        require_once '../app/models/' . $model . '.php';

        //Instantiate model class
        return new $model;
    }

    // Load view
    public function view($view, $data = []){
        // If view files exists, require it
        if (file_exists('../app/views/' . $view . '.php')){
            require_once '../app/views/' . $view . '.php';
        }   else{
            // View does not exist
            die('View doese not exist');
        }
    }
}