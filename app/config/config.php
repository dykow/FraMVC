<?php
/**
 * Config File
 * containing constant definitions
 */

// DB Params
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', '');


// App Root
define('APPROOT', dirname(dirname(__FILE__)));
// URL Root
define('URLROOT', 'http://localhost/FraMVC');
// Site Name
define('SITENAME', 'FraMVC');
// App Version
define('APPVERSION', '1.0');
